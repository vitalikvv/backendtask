const {errorHandler} = require('../middlewares/response.middleware');
const {Router} = require('express');
const FighterService = require('../services/fighterService');
const {responseMiddleware} = require('../middlewares/response.middleware');
const {createFighterValid, updateFighterValid} = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.post('/', [createFighterValid, errorHandler], function (req, res, next) {
    const result = FighterService.createNewFighter(req.body);
    if (result) {
        res.status(200).send(result)
    } else {
        next(new Error('Some error'));
    }
}, errorHandler);

router.get('/', function (req, res, next) {
    const result = FighterService.getAllFighters();
    if (result) {
        res.status(200).send(result)
    } else {
        next(new Error('Some error'));
    }
}, errorHandler);

router.get('/:id', function (req, res, next) {
    const fighterId = req.params.id
    const fighter = FighterService.search((item) => item.id === fighterId)
    if (fighter) {
        res.status(200).send(fighter)
    } else {
        next(new Error('Fighter not found'));
    }
}, errorHandler);

router.put('/:id', [updateFighterValid, errorHandler], function (req, res, next) {

    const fighterId = req.params.id
    const fighter = FighterService.search((item) => item.id === fighterId)
    let result

    if (fighter) {
        result = FighterService.update(fighterId, req.body)
    }

    if (result) {
        res.status(200).send(result)
    } else {
        next(new Error('Fighter not found'));
    }
}, errorHandler);
router.delete('/:id', function (req, res, next) {

    const fighterId = req.params.id
    const fighter = FighterService.search((item) => item.id === fighterId)
    let result

    if (fighter) {
        result = FighterService.delete(fighterId)
    }

    if (result) {
        res.status(200).json({
            error: false,
            message: `Fighter id: ${fighterId} deleted`,
        })
    } else {
        next(new Error('Fighter not found'));
    }
}, errorHandler);
// TODO: Implement route controllers for fighter

module.exports = router;