const {errorHandler} = require('../middlewares/response.middleware');
const {Router} = require('express');
const UserService = require('../services/userService');
const {createUserValid, updateUserValid} = require('../middlewares/user.validation.middleware');
const {responseMiddleware} = require('../middlewares/response.middleware');

const router = Router()

router.post('/', [createUserValid, errorHandler], function (req, res, next) {

    const result = UserService.createNewUser(req.body);
    if (result) {
        res.status(200).send(result)
    } else {
        next(new Error('Some error'));
    }
}, errorHandler);
router.get('/', function (req, res, next) {

    const users = UserService.getAllUsers()
    if (users) {
        res.status(200).send(users)
    } else {
        next(new Error('Some error'));
    }
}, errorHandler);
router.get('/:id', function (req, res, next) {
    const userId = req.params.id
    const user = UserService.search((item) => item.id === userId)
    if (user) {
        res.status(200).send(user)
    } else {
        next(new Error('User not found'));
    }
}, errorHandler);
router.put('/:id', [updateUserValid, errorHandler], function (req, res, next) {

    const userId = req.params.id
    const user = UserService.search((item) => item.id === userId)
    let result

    if (user) {
        result = UserService.update(userId, req.body)
    }

    if (result) {
        res.status(200).send(result)
    } else {
        next(new Error('User not found'));
    }
}, errorHandler);
router.delete('/:id', function (req, res, next) {

    const userId = req.params.id
    const user = UserService.search((item) => item.id === userId)
    let result

    if (user) {
        result = UserService.delete(userId)
    }

    if (result) {
        res.status(200).json({
            error: false,
            message: `User id: ${userId} deleted`,
        })
    } else {
        next(new Error('User not found'));
    }
}, errorHandler);

// TODO: Implement route controllers for user

module.exports = router;