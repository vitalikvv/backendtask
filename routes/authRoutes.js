const {Router} = require('express');
const AuthService = require('../services/authService');
const {responseMiddleware} = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {

        try {
            // TODO: Implement login action (get the user if it exist with entered credentials)
            AuthService.login((item) => item.email === req.body.email && item.password === req.body.password)
            next()
        } catch
            (err) {
            res.status(400).json({
                error: true,
                message: err.message,
            })
        }
    },
    responseMiddleware
)
;

module.exports = router;