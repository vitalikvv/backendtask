const {fighter} = require('../models/fighter');
const FighterService = require('../services/fighterService');

function checkNameFighters(array, targetOfSearch) {
    let i = array.length;
    while (i--) {
        if (array[i].name.toUpperCase() === targetOfSearch.toUpperCase()) {
            return true;
        }
    }
}

function checkFields(item) {
    return item === 'name' || item === 'power' || item === 'defense';
}

function checkNameForUpdate(name, fighterId, fighters) {
    if (name) {
        let i = fighters.length;
        while (i--) {
            if (fighters[i].name.toUpperCase() === name.toUpperCase() && fighters[i].id !== fighterId) {
                return false;
            }
        }
        return true
    } else {
        return true
    }
}

function checkPowerForUpdate(amntOfPower) {
    if (amntOfPower) {
        return typeof amntOfPower === "number" && amntOfPower > 1 && amntOfPower < 100
    } else {
        return true
    }
}
function checkDefenseForUpdate(amntOfDefense) {
    if (amntOfDefense) {
        return typeof amntOfDefense === "number" && amntOfDefense > 1 && amntOfDefense < 10
    } else {
        return true
    }
}

const createFighterValid = (req, res, next) => {
    const fieldOfRequest = Object.keys(req.body)
    const verificationTypeOfField = fieldOfRequest.every(checkFields);
    if (verificationTypeOfField) {
        if (req.body.name && req.body.power && req.body.defense) {
            const fighters = FighterService.getAllFighters()
            if (!checkNameFighters(fighters, req.body.name)) {//проверка чтобы не дублировались юзеры по name
                if (typeof req.body.power === "number" && typeof req.body.defense === "number") {
                    if (req.body.power > 1 && req.body.power < 100) {
                        if (req.body.defense > 1 && req.body.defense < 10) {
                            next();
                        } else {
                            next(new Error('Defense must be less 10 and more 1'));
                        }
                    } else {
                        next(new Error('Power must be less 100 and more 1'));
                    }
                } else {
                    next(new Error('Power and defense must be a number'));
                }

            } else {
                next(new Error('Fighter with such name is already registered'));
            }
        } else {
            next(new Error('All fields is required!'));
        }
    } else {
        next(new Error('Some field is redundant!'));
    }
    // TODO: Implement validatior for fighter entity during creation
}

const updateFighterValid = (req, res, next) => {
    const fighterId = req.params.id //для проверки на дубликаты, чтобы искать среди других fighters
    if (req.body.name?.length > 1 || req.body?.power > 1 || req.body?.defense > 1) {
        const fieldOfRequest = Object.keys(req.body)
        const verificationTypeOfField = fieldOfRequest.every(checkFields); //проверка, чтобы не было лишних полей
        if (verificationTypeOfField) {
            const fighters = FighterService.getAllFighters()
            if (checkNameForUpdate(req.body.name, fighterId, fighters) && checkPowerForUpdate(req.body.power) && checkDefenseForUpdate(req.body.defense)) {
                next()
            } else {
                next(new Error('Fighter entity to update isn"t valid'));
            }
        } else {
            next(new Error('Some field is redundant!'));
        }
    } else {
        next(new Error('Nothing to update!'));
    }
    // TODO: Implement validatior for fighter entity during update
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;