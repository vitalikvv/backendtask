const responseMiddleware = (req, res, next) => {
        res.status(200).json({
            error: false,
            message: `User ${req.body.email} authorized`,
        })

   // TODO: Implement middleware that returns result of the query
    next();
}

const errorHandler = (err, req, res, next) => {
    res.status(400).json({
        error: true,
        message: err.message,
    })
}

exports.responseMiddleware = responseMiddleware;
exports.errorHandler = errorHandler;