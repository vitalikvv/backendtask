const {user} = require('../models/user');
const UserService = require('../services/userService');

function checkEmailUsers(array, targetOfSearch) {
    let i = array.length;
    while (i--) {
        if (array[i].email.toUpperCase() === targetOfSearch.toUpperCase()) {
            return true;
        }
    }
}

function checkPhoneUsers(array, targetOfSearch) {
    let i = array.length;
    while (i--) {
        if (array[i].phoneNumber.toUpperCase() === targetOfSearch.toUpperCase()) {
            return true;
        }
    }
}

function checkFields(item) {
    return item === 'email' || item === 'password' || item === 'firstName' || item === 'phoneNumber' || item === 'lastName';
}

function checkPassword(password) {
    if (password) {
        return password.length >= 3;
    } else {
        return true
    }
}

function checkEmail(email) {
    if (email) {
        return email.toUpperCase().indexOf("@gmail.com".toUpperCase()) > 0;
    } else {
        return true
    }
}

function checkPhoneNumber(phoneNumber) {
    if (phoneNumber) {
        const number = phoneNumber.substr(1)*1
        if (number && typeof number === "number") {
            return phoneNumber.toUpperCase().indexOf("+380".toUpperCase()) === 0;
        }
    } else {
        return true
    }
}

function checkEmailUsersForUpdate(array, targetOfSearch, userId) {
    let i = array.length;
    while (i--) {
        if (array[i].email.toUpperCase() === targetOfSearch.toUpperCase() && array[i].id !== userId) {
            return true;
        }
    }
}

function checkPhoneUsersForUpdate(array, targetOfSearch, userId) {
    let i = array.length;
    while (i--) {
        if (array[i].phoneNumber.toUpperCase() === targetOfSearch.toUpperCase() && array[i].id !== userId) {
            return true;
        }
    }
}

const createUserValid = (req, res, next) => {
    const fieldOfRequest = Object.keys(req.body)
    const verificationTypeOfField = fieldOfRequest.every(checkFields);
    if (verificationTypeOfField) {
        if (req.body.email && req.body.password && req.body.firstName && req.body.lastName && req.body.phoneNumber) {
            if (req.body.email.toUpperCase().indexOf("@gmail.com".toUpperCase()) > 0) {
                if (checkPhoneNumber(req.body.phoneNumber)) {
                    if (req.body.password.length >= 3) {
                        const users = UserService.getAllUsers()
                        if (!checkEmailUsers(users, req.body.email)) {//проверка чтобы не дублировались юзеры по емейл
                            if (!checkPhoneUsers(users, req.body.phoneNumber)) {//проверка чтобы не дублировались юзеры по телефону
                                next()
                            } else {
                                next(new Error('User with such phone is already registered'));
                            }
                        } else {
                            next(new Error('User with such mail is already registered'));
                        }
                    } else {
                        next(new Error('Password need longer 2 symbols'));
                    }
                } else {
                    next(new Error('You must have phone number - +380xxxxxxxxx'));
                }
            } else {
                next(new Error('You must specify email - gmail.com'));
            }
        } else {
            next(new Error('All fields is required!'));
        }
    } else {
        next(new Error('Some field is redundant!'));
    }
    // TODO: Implement validatior for user entity during creation
}

const updateUserValid = (req, res, next) => {
    const userId = req.params.id //для проверки на дубликаты, чтобы искать среди других юзеров
    if (req.body.email?.length > 1 || req.body.password?.length > 1 || req.body.firstName?.length > 1 || req.body.lastName?.length > 1 || req.body.phoneNumber?.length > 1) {
        const fieldOfRequest = Object.keys(req.body)
        const verificationTypeOfField = fieldOfRequest.every(checkFields); //проверка, чтобы не было лишних полей
        if (verificationTypeOfField) {
            if (checkPassword(req.body.password) && checkEmail(req.body.email) && checkPhoneNumber(req.body.phoneNumber)) {
                const users = UserService.getAllUsers()
                if (!checkEmailUsersForUpdate(users, req.body.email, userId)) {//проверка чтобы не дублировались юзеры по емейл
                    if (!checkPhoneUsersForUpdate(users, req.body.phoneNumber, userId)) {//проверка чтобы не дублировались юзеры по телефону
                        next()
                    } else {
                        next(new Error('User with such phone is already registered'));
                    }
                } else {
                    next(new Error('User with such mail is already registered'));
                }
            } else {
                next(new Error('User entity to update isn"t valid'));
            }
        } else {
            next(new Error('Some field is redundant!'));
        }
    } else {
        next(new Error('Nothing to update!'));
    }
    // TODO: Implement validatior for user entity during update

}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;