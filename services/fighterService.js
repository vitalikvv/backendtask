const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    getAllFighters() {
        const items = FighterRepository.getAll()
        return items;
    }
    createNewFighter(fighter) {
        const item = FighterRepository.create(fighter)
        if(!item) {
            return null;
        }
        return item;
    }
    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
    update(id, dataToUpdate) {
        const item = FighterRepository.update(id, dataToUpdate);
        if(!item) {
            return null;
        }
        return item;
    }
    delete(id) {
        const item = FighterRepository.delete(id);
        if(!item) {
            return null;
        }
        return item;
    }

    // TODO: Implement methods to work with fighters
}

module.exports = new FighterService();