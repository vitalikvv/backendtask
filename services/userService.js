const { UserRepository } = require('../repositories/userRepository');

class UserService {
    createNewUser(user) {
        const item = UserRepository.create(user)
        if(!item) {
            return null;
        }
        return item;
    }
    getAllUsers() {
        const items = UserRepository.getAll()
        if(!items) {
            return null;
        }
        return items;
    }
    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
    update(id, dataToUpdate) {
        const item = UserRepository.update(id, dataToUpdate);
        if(!item) {
            return null;
        }
        return item;
    }
    delete(id) {
        const item = UserRepository.delete(id);
        if(!item) {
            return null;
        }
        return item;
    }
    // TODO: Implement methods to work with user
}

module.exports = new UserService();